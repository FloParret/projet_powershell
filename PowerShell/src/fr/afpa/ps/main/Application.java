package fr.afpa.ps.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * 
 * Cette classe permet l'affichage et l'orientation des differentes commandes de
 * l'utilisateur
 * 
 * @author Flo & Brahim
 * @version 1.0
 */
public class Application {
	/**
	 * Cette variable static est l'entr�e clavier de l'utilisateur, elle est utilis�
	 * dans presque toutes les m�thodes de l'application pour permettre les
	 * diff�rents traitement de celle ci. 
	 */
	public static String currentEntry;
	/**
	 * Cette liste permet le d�coupage de l'entr�e de l'utilisateur afin d'isoler les �l�ments.
	 * @see currentEntry
	 * @see getTokens()
	 */
	static List<String> tokens = new ArrayList<>();

	public static void main(String[] args) {
		Application p = new Application();
		Commande c = new Commande();

		Scanner sc = new Scanner(System.in);
		System.out.println(
				"------------------------------------------------------------------------------------------------------------------");
		System.out.println("Bienvenue dans l'invite de commande MyPowerShell !");

		while (true) {
			System.out.print(Commande.currentPath + " : ");
			Application.currentEntry = sc.nextLine();
			p.getTokens(currentEntry);
			String choix = Application.tokens.get(0);

			switch (choix) {
			case "MyCd":
				c.cdCmd();
				break;
			case "MyPwd":
				c.pwdCmd();
				break;
			case "MyMv":
				c.mvCmd();
				break;
			case "MyRm":
				c.rmCmd();
				break;
			case "MyCp":
				c.cpCmd();
				break;
			case "MyLs":
				c.lsCmd();
				break;
			case "MyFind":
				c.findCmd();
				break;
			case "MyPs":
				c.psCmd();
				break;
			case "MyDisplay":
				c.displayCmd();
				break;
			case "MyHelp":
				c.helpCmd();
				break;
			default:
				System.out.println(
						"Commande non valide, tapez 'MyHelp' pour acc�der � la liste des commandes utilisable.");
				break;
			}
		}

	}
	/**
	 * Cette methode permet le d�coupage de l'entr�e de l'utilisateur afin d'isoler les �l�ments.
	 * @see currentEntry
	 * @see tokens
	 */

	public List<String> getTokens(String str) {
		tokens.removeAll(tokens);
		StringTokenizer tokenizer = new StringTokenizer(str, " ");
		while (tokenizer.hasMoreElements()) {
			tokens.add(tokenizer.nextToken());
		}
		return tokens;
	}
}
