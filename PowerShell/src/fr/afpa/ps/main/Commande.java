package fr.afpa.ps.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
/**
 * Cette classe est le moteur de l'application, elle contient tout les commandes.
 * 
 * @author Flo & Brahim
 * @version 1.0
 */
public class Commande {
    /**
     * Le chemin courant de l'utilisateur. Cette variable est modifiable par la commande MyCd.
     * 
     * @see cdCmd()
     * 
     */
	static Path currentPath = Paths.get(System.getProperty("user.home"));
	
	/**
     * Une instance de la classe StringMatcher
     */
	StringMatcher stringMatcher = new StringMatcher();
	/**
     * Une instance de la classe Application
     */
	Application powerShell = new Application();
	/**
     * Cette m�thode affiche le chemin en cours
     */
	public void pwdCmd() {
		System.out.println("Vous vous trouvez dans : " + currentPath);
	}
	/**
     * Cette m�thode permet de naviguer dans les dossiers, elle modifie le currentPath en fonction des choix de l'utilisateur
     * 
     * @see CurrentPath
     * @see selectNavigationCmd
     */
	public void cdCmd() {
		int numeroCommande = stringMatcher.selectNavigationCmd(Application.currentEntry);
		if (numeroCommande == 1) {
			try {

				Path tmpPath = Paths.get(Application.currentEntry.replace("MyCd ", ""));
				if (!tmpPath.isAbsolute()) {
					tmpPath = Paths.get(currentPath + "/" + Application.tokens.get(1));

				}
				if (tmpPath.toFile().exists()) {
					currentPath = tmpPath.toAbsolutePath();
				} else {
					System.out.println("Le dossier n'existes pas.");
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		} else if (numeroCommande == 2) {
			currentPath = currentPath.getParent();

		} else if (numeroCommande == 3) {
			currentPath = Paths.get(System.getProperty("user.home"));

		}
	}
	/**
     * Cette m�thode permet de deplacer des fichiers ou de les renommer selon la syntaxe utilis� par l'utilisateur
     * 
     * @see MvNavigationCmd
     */
	public void mvCmd() {
		int numeroCommande = stringMatcher.selectMvCmd(Application.currentEntry);

		if (numeroCommande == 1) {

			File source = new File(Application.tokens.get(1));
			File destination = new File(Application.tokens.get(2));
		


			if (!source.isAbsolute()) {
				source = new File(currentPath + "/" + Application.tokens.get(1));
			}
			if (!destination.isAbsolute()) {
				destination = new File(currentPath + "/" + Application.tokens.get(2));
			}
			File destinationWithSourceFile = new File(destination + "/" + source.getName());
			if (source.exists() && destination.exists()) {

				try {
					Files.copy(source.toPath(), destinationWithSourceFile.toPath(), REPLACE_EXISTING);
					source.delete();
					System.out.println("D�placement de " + source.getName() + " dans le dossier " + destination);
				} catch (IOException e) {
					System.err.println(e);
				} catch (Exception e) {
					e.printStackTrace();

				}

			} else if (source.exists() && !destination.exists()) {
				try {
					if (destination.mkdir()) {

						System.out.println("Cr�ation d'un nouveau dossier : " + destination);
					} else {

						System.out.println("Cr�ation de " + destination + " impossible.");
					}
					Files.copy(source.toPath(), destinationWithSourceFile.toPath(), REPLACE_EXISTING);
					source.delete();
					System.out.println("D�placement de " + source.getName() + " dans le dossier " + destination);
				} catch (IOException e) {
					System.err.println(e);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("Erreur, veuillez vous d�placer dans le bon dossier.");
			}
		}
		if (numeroCommande == 2) {
			File oldFile = new File(Application.tokens.get(1));
			File newFile = new File(Application.tokens.get(2));
			if (!oldFile.isAbsolute()) {
				oldFile = new File(currentPath + "/" + Application.tokens.get(1));
			}
			if (!newFile.isAbsolute()) {
				newFile = new File(currentPath + "/" + Application.tokens.get(2));
			}

			try {
				Files.copy(oldFile.toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
				oldFile.delete();
				System.out.println("Renamed " + oldFile.getName() + " in " + newFile);
			} catch (IOException e) {
				System.err.println(e);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
     * Cette m�thode permet de copier des fichiers selon la syntaxe utilis� par l'utilisateur
     * 
     * @see CpNavigationCmd
     */

	public void cpCmd() {
		int numeroCommande = stringMatcher.selectCpCmd(Application.currentEntry);
		if (numeroCommande == 1) {

			File source = new File(Application.tokens.get(1));
			File destination = new File(Application.tokens.get(2));
		


			if (!source.isAbsolute()) {
				source = new File(currentPath + "/" + Application.tokens.get(1));
			}
			if (!destination.isAbsolute()) {
				destination = new File(currentPath + "/" + Application.tokens.get(2));
			}
			File destinationWithSourceFile = new File(destination + "/" + source.getName());
			if (source.exists() && destination.exists()) {

				try {
					Files.copy(source.toPath(), destinationWithSourceFile.toPath(), REPLACE_EXISTING);
					System.out.println("Copie de " + source.getName() + " dans le dossier " + destination);
				} catch (IOException e) {
					System.err.println(e);
				} catch (Exception e) {
					e.printStackTrace();

				}
			} else {
				System.out.println(
						"Commande impossible : Le dossier de destination ou le fichier � copier n'existe pas.");
			}

		}
		if (numeroCommande == 2) {
			File source = new File(Application.tokens.get(2));
			File destination = new File(Application.tokens.get(3));
			if (!source.isAbsolute()) {
				source = new File(currentPath + "/" + Application.tokens.get(2));
			}
			if (!destination.isAbsolute()) {
				destination = new File(currentPath + "/" + Application.tokens.get(3));
			}
			if (source.exists()) {
				try {
					copyFolder(source, destination);
				} catch (IOException e) {
					e.printStackTrace();

				}
			} else {
				System.out.println("Le dossier n'existes pas.");
			}
		}
	}
	/**
     * Cette m�thode permet lister tout les fichiers dans le repertoire en cours
     * 
     * @CurrentPath
     */

	public void lsCmd() {

		String directory = currentPath.toString();
		File[] files = new File(directory).listFiles();
		String repertoire = "-";
		String fichier = "-";
		String read = "-";
		String hidden = "-";
		String write = "-";
		String execute = "-";

		if (files != null && (files.length != 0)) {
			System.out.printf("%-35s %-35s %-35s %-35s %n", "Mode ", "LastWriteTime", "Size", "Name");
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			for (File file : files) {
				if (file.isFile() || file.isDirectory()) {
					if (file.isDirectory() == true) {
						repertoire = "d";
					}
					if (file.isFile() == true) {
						fichier = "a";
					}
					if (file.canRead() == true) {
						read = "r";
					}
					if (file.isHidden() == true) {
						hidden = "h";
					}
					if (file.canWrite() == true) {
						write = "w";
					}
					if (file.canExecute() == true) {
						execute = "e";
					}
					String mode = repertoire + fichier + read + hidden + write + execute;
					SimpleDateFormat dateModif = new SimpleDateFormat("dd/MM/yyyy HH:mm");
					System.out.printf("%-35s %-35s %-35s %-35s %n", mode, dateModif.format(file.lastModified()),
							file.length() + " ko", file.getName());
					repertoire = "-";
					fichier = "-";
					read = "-";
					hidden = "-";
					write = "-";
					execute = "-";
				}
			}
		} else {
			System.out.println("Le dossier est vide..");
		}
	}
	/**
     * Cette m�thode permet de supprimer des fichiers en fonction de la syntaxe de l'utilisateur
     * 
     * @selectRmCmd
     */
	public void rmCmd() {
		int numeroCommande = stringMatcher.selectRmCmd(Application.currentEntry);
		Scanner demandeValidationDeSuppression = new Scanner(System.in);
		if (numeroCommande == 1) {
			File source = new File(currentPath.toString());
			File[] files = source.listFiles();
			System.out.println("Voulez-vous vraiment supprimer ces fichiers (O ou N) ?");
			String ValidationDeSuppression = demandeValidationDeSuppression.nextLine();
			ValidationDeSuppression = ValidationDeSuppression.toUpperCase();
			if (files != null && (files.length != 0) && ValidationDeSuppression.equals("O")) {
				for (File file : files) {
					if (file.toString().endsWith(Application.tokens.get(1).replaceAll("\\*", ""))) {
						try {
							Files.delete(file.toPath());
							System.out.println(file + " a �t� supprim�.");
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

				}
			} else {
				System.out.println("Suppression annule");
			}
		}
		if (numeroCommande == 2) {
			Integer i = (Application.tokens.size()) - 1;
			System.out.println("Voulez-vous vraiment supprimer ce(s) fichier(s) (O ou N) ?");
			String ValidationDeSuppression = demandeValidationDeSuppression.nextLine();
			ValidationDeSuppression = ValidationDeSuppression.toUpperCase();
			if (ValidationDeSuppression.equals("O")) {
				do {
					File source = new File(Application.tokens.get(i));
					if (!source.isAbsolute()) {
						source = new File(currentPath + "/" + Application.tokens.get(i));
					}
					if (source.exists()) {
						try {
							if (!Application.tokens.get(i).contains("MyRm")) {
								Files.delete(source.toPath());
								System.out.println("Suppresion de " + source.getName());
								i--;
							} else
								break;
						} catch (IOException e) {
							System.err.println(e);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						System.out.println("Commande impossible : Le dossier ou le fichier � supprimer n'existe pas.");
					}

				} while (Application.tokens.get(i) != Application.tokens.get(0));

			} else {
				System.out.println("Suppression annule");
			}
		}
		if (numeroCommande == 3) {
			File source = new File(Application.tokens.get(2));

			if (!source.isAbsolute()) {
				source = new File(currentPath + "/" + Application.tokens.get(2));
			}

			System.out.println("Voulez-vous vraiment supprimer ce fichier (O ou N) ?");
			String ValidationDeSuppression = demandeValidationDeSuppression.nextLine();
			ValidationDeSuppression = ValidationDeSuppression.toUpperCase();
			if (ValidationDeSuppression.equals("O")) {
				if (source.exists() && source.isDirectory()) {
					try {
						delete(source);
						System.out.println("Suppresion de " + source.getName());
					} catch (IOException e) {
						System.err.println(e);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					System.out.println("Commande impossible : Le dossier ou le fichier � supprimer n'existe pas.");
				}
			} else {
				System.out.println("Suppression annule");
			}
		}

	}
	/**
     * Cette m�thode permet de lire des fichiers.
     * 
     * 
     */

	public void displayCmd() {
		int numeroCommande = stringMatcher.selectDisplayCmd(Application.currentEntry);
		if (numeroCommande == 1) {
			File file = new File(Application.tokens.get(1));
			if (!file.isAbsolute()) {
				file = new File(currentPath + "/" + Application.tokens.get(1));
			}
			if (file.exists()) {
				try {
					BufferedReader readText = new BufferedReader(new FileReader(file));

					String st;
					System.out.println();
					while ((st = readText.readLine()) != null) {
						System.out.println(st);
					}
					readText.close();
				} catch (IOException e) {
					System.err.println(e);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			System.out.println();
		}
		if (numeroCommande == 2) {
			File file = new File(Application.tokens.get(1));
			if (!file.isAbsolute()) {
				file = new File(currentPath + "/" + Application.tokens.get(1));
			}
			if (file.exists()) {
				try {
					BufferedReader readtext = new BufferedReader(new FileReader(file));
					String st;
					while ((st = readtext.readLine()) != null) {
						System.out.println(st);
					}
					readtext.close();
				} catch (IOException e) {
					System.err.println(e);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	/**
     * Cette m�thode permet d'afficher une aide pour toutes les commandes de l'application.
     */

	public void helpCmd() {
		String nomCommande = "Nom de la commande";
		String roleDeLaCommande = "R�le de la commande";
		System.out.println();
		System.out.printf("\t\t\t\t\t\t\t\t\t\t\t\t %-80s %n", "Les commandes de notre Powershell");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println();
		System.out.printf("%-75s %-35s %n", nomCommande, roleDeLaCommande);
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-56s %-35s %n", "MyPwd",
				"Affiche le chemin absolu de l'endroit o� se trouve l'utilisateur");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-56s %-35s %n", "MyCd ou MyCd ~", "Acc�der directement au r�pertoire de l'utilisateur.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-56s %-35s %n", "MyCd /", "Permet de se retrouver � la racine du disque.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyCd path", "Aller dans le r�pertoire en entrant son chemin absolu.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyCd ..",
				"Remonter dans le r�pertoire parent � partir de l�  o� vous �tes.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyCp foo/bar.txt baz/",
				"Copier le fichier bar.txt dans le r�pertoire baz. Si baz n'existe pas alors il sera cr��.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyCp -r foo/ baz/", "Copier des r�pertoires entiers.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyMv foo/bar.txt baz/",
				"D�placer le fichier bar.txt dans le r�pertoire baz.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyMv foo/foo_bar.txt baz/foo_baz.txt",
				"D�placer le fichier bar.txt dans le r�pertoire baz et le renomme baz.txt. Si baz n'existe pas alors il sera cr��.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyRm *.txt", "Supprimer tous les fichiers ayant pour extension txt.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyRm foo.txt bar.txt", "Supprimer les fichiers foo.txt et bar.txt.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyRm -rf baz/", "Supprimer le r�pertoire baz et tout son contenu.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyHelp",
				"Afficher l'ensemble des commandes ainsi que leur fonctions respectives.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyDisplay test.txt",
				"Afficher le contenu du fichier test.txt (fonctionne aussi avec un fichier .html etc..).");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyLs",
				"Afficher le nom, la taille, le mode (permissions), la date de derni�re modification des dossiers et fichiers.");
		System.out.println(
				"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyFind *app",
				"Rechercher et afficher l'ensemble des fichiers qui se terminent par app.");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyFind app*",
				"Rechercher et afficher l'ensemble des fichiers qui commencent par app");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyFind *app*",
				"Rechercher et afficher l'ensemble des fichiers qui contiennent app");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyFind -R *app* ou Myfind -R *app ou Myfind -R app* ",
				"Rechercher et afficher respectivement l'ensemble des fichiers dans le dossier en cours et les sous dossier qui contiennent, se terminent ou commencent par app");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.printf("%-55s  %-35s %n", "MyHelp",
				"Afficher l'ensemble des commandes disponibles ainsi que le descriptif de leur r�le");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println();
	}
	/**
     * Cette m�thode permet de cherche n'importe quelle fichier en fonction d'un filtre, elle utilise une m�thode de recherche r�cursive.
     * @see listFilesAndSubpath
     * @see listFilesOfOneDirectory
     */
	public void findCmd() {
		int numeroCommande = stringMatcher.selectFindCmd(Application.currentEntry);
		String filtre = (Application.tokens.get(1).equals("-R")) ? Application.tokens.get(2) : Application.tokens.get(1);
		filtre = filtre.replace("*", "").toLowerCase();
		File folder = new File(currentPath.toString());
		System.out.printf("%-70s %-70s %-70s %n", "LastWriteTime ", "Repository", "Name");

		System.out.println(
				"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		if (Application.tokens.get(1).equals("-R")) {
			listFilesAndSubpath(numeroCommande, filtre, folder);
		} else {
			listFilesOfOneDirectory(numeroCommande, filtre, folder);
		}
		System.out.println();

	}
	/**
     * Cette m�thode permet d'afficher la liste des processus du systeme
     * 
     */
	public void psCmd() {
		try {
			String line;
			Process p = Runtime.getRuntime().exec(System.getenv("windir") + "\\system32\\" + "tasklist.exe");
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				System.out.println(line); // <-- Parse data here.
			}
			input.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}
	/**
     * Cette m�thode permet de lister de fa�on r�cursive tout les fichiers d'un dossier et de des sous dossiers
     * @findCmd()
     * 
     * @param numeroCommande la commande identifi� par StringMatcher
     * @param filtre le filtre utilis� pour la recherche
     * @param folder le dossier � partir duquel la recherche s'effectue
     * 
     */
	public void listFilesAndSubpath(int numeroCommande, String filtre, File folder) {

		SimpleDateFormat dateModif = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		File[] listOfFiles = folder.listFiles();
		int i = 0;
		if (listOfFiles != null)
			for (File file : listOfFiles) {

				if (file.isDirectory()) {
					listFilesAndSubpath(numeroCommande, filtre, file);
				}
				if (numeroCommande == 1 && file.getName().toString().toLowerCase().endsWith(filtre.toString())) {
					System.out.printf("%-70s %-70s %-70s %n", dateModif.format(file.lastModified()),
							file.toPath().subpath(file.toPath().getNameCount() - 3, file.toPath().getNameCount() - 1),
							file.getName());
				}
				if (numeroCommande == 2 && file.getName().toString().toLowerCase().startsWith(filtre.toString())) {

					System.out.printf("%-70s %-70s %-70s %n", dateModif.format(file.lastModified()),
							file.toPath().subpath(file.toPath().getNameCount() - 3, file.toPath().getNameCount() - 1),
							file.getName());
				}
				if (numeroCommande == 3 && file.getName().toString().toLowerCase().contains(filtre.toString())) {

					System.out.printf("%-70s %-70s %-70s %n", dateModif.format(file.lastModified()),
							file.toPath().subpath(file.toPath().getNameCount() - 3, file.toPath().getNameCount() - 1),
							file.getName());
				}
				if (i == listOfFiles.length - 1) {
					break;
				}
				i++;
			}
	}
	/**
     * Cette m�thode permet de lister les fichiers d'un dossier selon un filtre pr�d�fini
     * @findCmd()
     * 
     * @param numeroCommande la commande identifi� par StringMatcher
     * @param filtre le filtre utilis� pour la recherche
     * @param folder le dossier � partir duquel la recherche s'effectue
     * 
     */
	public void listFilesOfOneDirectory(int numeroCommande, String filtre, File folder) {

		SimpleDateFormat dateModif = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		File[] listOfFiles = folder.listFiles();
		if (listOfFiles != null)
			for (File file : listOfFiles) {

				if (numeroCommande == 1 && file.getName().toString().toLowerCase().endsWith(filtre.toString())) {
					System.out.printf("%-70s %-70s %-70s %n", dateModif.format(file.lastModified()),
							file.toPath().subpath(file.toPath().getNameCount() - 3, file.toPath().getNameCount() - 1),
							file.getName());
				}
				if (numeroCommande == 2 && file.getName().toString().toLowerCase().startsWith(filtre.toString())) {

					System.out.printf("%-70s %-70s %-70s %n", dateModif.format(file.lastModified()),
							file.toPath().subpath(file.toPath().getNameCount() - 3, file.toPath().getNameCount() - 1),
							file.getName());
				}
				if (numeroCommande == 3 && file.getName().toString().toLowerCase().contains(filtre.toString())) {

					System.out.printf("%-70s %-70s %-70s %n", dateModif.format(file.lastModified()),
							file.toPath().subpath(file.toPath().getNameCount() - 3, file.toPath().getNameCount() - 1),
							file.getName());
				}

			}
	}
	/**
     * Cette m�thode permet de copier les fichiers d'un repertoire de maniere recursive
     * @MyCp()
     * 
     * @param src le dossier source
     * @param dest le dossier de destination
     * 
     */

	public void copyFolder(File src, File dest) throws IOException {

		if (src.isDirectory()) {

			// if directory not exists, create it
			if (!dest.exists()) {
				dest.mkdir();
				System.out.println("Directory copied from " + src + "  to " + dest);
			}

			// list all the directory contents
			String files[] = src.list();

			for (String file : files) {
				// construct the src and dest file structure
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				// recursive copy
				copyFolder(srcFile, destFile);
			}

		} else {
			// if file, then copy it
			// Use bytes stream to support all file types
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dest);

			byte[] buffer = new byte[1024];

			int length;
			// copy the file content in bytes
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}

			in.close();
			out.close();
			System.out.println("File copied from " + src + " to " + dest);
		}
	}
	/**
     * Cette m�thode permet d'effacer un dossier et de supprimer les fichiers a l'interieur si il n'es pas vide
     * @MyRm()
     * 
     * @param file le dossier qu'on veut supprimer
     */

	public void delete(File file) throws IOException {

		for (File childFile : file.listFiles()) {

			if (childFile.isDirectory()) {
				delete(childFile);
			} else {
				if (!childFile.delete()) {
					throw new IOException();
				}
			}
		}

		if (!file.delete()) {
			throw new IOException();
		}
	}

}
